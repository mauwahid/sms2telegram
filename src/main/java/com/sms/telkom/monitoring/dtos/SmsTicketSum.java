package com.sms.telkom.monitoring.dtos;

import lombok.Data;

@Data
public class SmsTicketSum {

    private int countTicketNum;

    private int answeredYes;

    private int answeredNo;

    private int noResponse;
}
