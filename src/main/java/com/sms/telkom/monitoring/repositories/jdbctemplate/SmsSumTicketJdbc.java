package com.sms.telkom.monitoring.repositories.jdbctemplate;

import com.sms.telkom.monitoring.dtos.SmsTicketSum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class SmsSumTicketJdbc {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private class SmsSumTicketMapper implements RowMapper<SmsTicketSum> {

        @Override
        public SmsTicketSum mapRow(ResultSet resultSet, int i) throws SQLException {

            SmsTicketSum smsTicketSum = new SmsTicketSum();
            smsTicketSum.setAnsweredYes(resultSet.getInt("answered_yes"));
            smsTicketSum.setAnsweredNo(resultSet.getInt("answered_no"));
            smsTicketSum.setCountTicketNum(resultSet.getInt("count_ticket_num"));
            smsTicketSum.setNoResponse(resultSet.getInt("no_response"));


            return smsTicketSum;
        }
    }

    public SmsTicketSum findTodayData() {


        return jdbcTemplate.queryForObject("select (select count(*) from sms_resolved_active where date(date_created) = curdate()) as count_ticket_num ,\n" +
                "(select count(*) from sms_resolved_active where answer like 'Y' and date(date_created) = curdate())  as answered_yes ,\n" +
                "(select count(*) from sms_resolved_active where answer like 'N' and date(date_created) = curdate())  as answered_no,\n" +
                "(select count(*) from sms_resolved_active where answer is null and date(date_created) = curdate())  as no_response\n", new SmsSumTicketMapper()
        );

    }

    public SmsTicketSum findLastHour() {


        return jdbcTemplate.queryForObject("select (select count(*) from sms_resolved_active where date_created >= DATE_SUB(NOW(),INTERVAL 1 HOUR)\n" +
                " and date_created <= NOW()) as count_ticket_num ,\n" +
                "(select count(*) from sms_resolved_active where answer like 'Y' and last_updated >= DATE_SUB(NOW(),INTERVAL 1 HOUR)\n" +
                "and last_updated <= NOW())  as answered_yes ,\n" +
                "(select count(*) from sms_resolved_active where answer like 'N' and last_updated >= DATE_SUB(NOW(),INTERVAL 1 HOUR)\n" +
                "and last_updated <= NOW())  as answered_no,\n" +
                "(select count(*) from sms_resolved_active where answer is null and date(date_created) = curdate()) as no_response\n", new SmsSumTicketMapper()
        );

    }


}