package com.sms.telkom.monitoring.service;

import com.sms.telkom.monitoring.utils.Common;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class CheckingService {


    private String urlApp = "http://localhost:8080/";
    private String urlJob = "http://localhost:8081/";
    private String urlRabbitMQ = "http://localhost:15672/";


    public String cekStatusApp(){
        int status = 404;
        try{
            status = checkStatus(urlApp);
        }catch (Exception e){
            log.error(e.toString());
        }

        log.debug("App status code "+status);

        if(status == 200 || status == 302){
            return "SERVER_UP";
        }else{
            return "SERVER_DOWN";
        }
    }

    public String cekStatusJob(){
        int status = 404;
        try{
            status = checkStatus(urlJob);
        }catch (Exception e){
            log.error(e.toString());
        }

        log.debug("App status code "+status);

        if(status == 200){
            return "SERVER_UP";
        }else{
            return "SERVER_DOWN";
        }
    }

    public String cekStatusRabbit(){
        int status = 404;
        try{
            status = checkStatus(urlRabbitMQ);
        }catch (Exception e){
            log.error(e.toString());
        }

        log.debug("App status code "+status);

        if(status == 200){
            return "SERVER_UP";
        }else{
            return "SERVER_DOWN";
        }
    }



    private  int checkStatus(String uri)
            throws IOException {

        //  OkHttpClient client = new OkHttpClient();
        log.debug("URI "+uri.toString());
        // uri = uri+"lion/search/best_price";
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        OkHttpClient client = builder.build();

        HttpUrl.Builder urlBuilder = HttpUrl.parse(uri).newBuilder();

        String url = urlBuilder.build().toString();

      //  log.debug("url : "+url);

        Request request = new Request.Builder().
                url(url).build();

        try (Response response = client.newCall(request).execute()) {
            return response.code();
        }
    }




}
