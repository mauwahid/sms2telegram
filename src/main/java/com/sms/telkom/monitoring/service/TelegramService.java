package com.sms.telkom.monitoring.service;

import com.sms.telkom.monitoring.dtos.SmsTicketSum;
import com.sms.telkom.monitoring.repositories.jdbctemplate.SmsSumTicketJdbc;
import com.sms.telkom.monitoring.utils.OkHttpUtil;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class TelegramService {


    @Autowired
    private SmsSumTicketJdbc smsSumTicketJdbc;

    @Autowired
    private CheckingService checkingService;

    private String teleURL = "https://10.194.176.109:88/API/Broadcast/";
//    private String teleURL = "https://103.252.163.119:88/API/Broadcast/";




    public void runningService(){

        String statusApp = checkingService.cekStatusApp();
        String statusJob = checkingService.cekStatusJob();
        String statusRabbit = checkingService.cekStatusRabbit();

        SmsTicketSum smsTicketSum = smsSumTicketJdbc.findLastHour();
        SmsTicketSum smsTicketSumDay = smsSumTicketJdbc.findTodayData();

        String html = "STATUS SERVICE \n";
        html = html + "Status Application : "+statusApp+" \n";
        html = html + "Status Job Report : "+statusJob+" \n";
        html = html + "Status Rabbit MQ : "+statusRabbit+" \n";
        html = html +"\n \n";
        html = html + "SMS Last 1 Hour \n";
        html = html +"Ticket Count : "+smsTicketSum.getCountTicketNum()+"\n";
        html = html +"Answer Yes : "+smsTicketSum.getAnsweredYes()+"\n";
        html = html +"Answer No : "+smsTicketSum.getAnsweredNo()+"\n \n";

        html = html + "SMS Today \n";
        html = html +"Ticket Count : "+smsTicketSumDay.getCountTicketNum()+"\n";
        html = html +"Answer Yes : "+smsTicketSumDay.getAnsweredYes()+"\n";
        html = html +"Answer No : "+smsTicketSumDay.getAnsweredNo()+"\n";
        html = html +"Unanswered : "+smsTicketSumDay.getNoResponse()+"\n";

     //   html = "test";

        sendToTelegram(html);

    }


    private void sendToTelegram(String html){

        Map params = new HashMap<String,String>();
        params.put("KeyPublic","10004");
        params.put("App","mediacare");
        params.put("Message",html);

        //'Obelix#{KeyPrivate}#{Message}#{App}#Broadcast#AlertBOT'
        String hash = sha1("Obelix#40001#"+html+"#mediacare#Broadcast#AlertBOT");

        params.put("Hash",hash.toLowerCase());

        String response = "";

        try {
            response = sendRequest(teleURL, params);
        } catch (IOException e) {
            log.error("Send BOT Error "+e.toString());
        }


        log.debug("Response Bot "+response);
    }

/*
    public String sendRequest(String uri, Map<String,String> params) throws IOException {

        // OkHttpClient client = new OkHttpClient();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);

        OkHttpClient client = builder.build();

        FormBody.Builder formBuilder = new FormBody.Builder();

        params.forEach((k,v)->
                {
                    formBuilder.add(k,v);
                    // logger.debug("k "+k+", v ");
                }
        );

        log.debug("URI "+uri);

        Request request = new Request.Builder().url(uri).
                post(formBuilder.build()).build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }
*/
    public String sendRequest(String uri, Map<String,String> params) throws IOException {

        // OkHttpClient client = new OkHttpClient();

      /*  ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                .build();
*/
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
   //     builder.readTimeout(60, TimeUnit.SECONDS);
   //     builder.writeTimeout(60, TimeUnit.SECONDS);
        //  builder.connectionSpecs(Collections.singletonList(spec));

        OkHttpUtil.configureToIgnoreCertificate(builder);
//        OkHttpClient client = builder.build();

        OkHttpClient client = OkHttpUtil.configureToIgnoreCertificate(builder).build();

        Headers.Builder headerBuilder = new Headers.Builder();



        Headers headers = headerBuilder.build();

        FormBody.Builder formDataBuilder = new FormBody.Builder();
        //logger.debug("formBuilder "+formDataBuilder);

        //logger.debug("header param : "+headerParam);
        params.forEach((x,y)->
                {
                    try{
                        //              logger.debug("data : "+x+", "+y);

                        if(y!=null)
                            formDataBuilder.add(x,y);

                    }catch (Exception ex){
                        log.debug("exception "+ex.toString());
                    }

                }
        );

        log.debug("URL Call "+uri);
        log.debug("Param "+params.toString());

        Request request = new Request.Builder().
                url(uri).
                headers(headers).
                post(formDataBuilder.build()).build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }




    public String sha1(String input) {
        String sha1 = null;
        try {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
            msdDigest.update(input.getBytes("UTF-8"), 0, input.length());
            sha1 = DatatypeConverter.printHexBinary(msdDigest.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            log.debug("Digest failed " +e.toString());
        }
        return sha1;
    }







}
