package com.sms.telkom.monitoring;

import com.sms.telkom.monitoring.service.ArchivingService;
import com.sms.telkom.monitoring.service.TelegramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class Application {

	@Autowired
	private TelegramService smsToTelkomJobService;

	@Autowired
	private ArchivingService archivingService;

	public static void main(String[] args) {
		SpringApplication.run(com.sms.telkom.monitoring.Application.class, args);
	}


	@Scheduled(fixedDelay = 3600000)
	public void startJob(){
		smsToTelkomJobService.runningService();
	}



}
